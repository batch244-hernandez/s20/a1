/*
	Create a variable number that will store the value of the number provided by the user via prompt.

*/	let input = Number(prompt("Input a number."));
	console.log("The number you provided is " + input + ".");

/*
	Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
*/

	for (let number = input; number >= 0; number--){

		// Create a condition that if the current value is less than or equal to 50, stop the loop.
		if (number <= 50){
			console.log("The current value is at 50. Terminating the loop.");
			break;

		// Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
		} else if (number % 10 === 0){
			console.log("The number is divisible by 10. Skipping the number.");	
			continue;

		// Create another condition that if the current value is divisible by 5, print the number.
		} else if (number % 5 === 0){
			console.log(number);	
		};	
	};

/*
	Create a variable that will contain the string supercalifragilisticexpialidocious.
*/
	let word = "supercalifragilisticexpialidocious";
	console.log(word);

	// Create another variable that will store the consonants from the string.
	let cons = "";

	// Create another for loop that will iterate through the individual letters of the string based on its length.
	for (let i = 0; i < word.length; i++){

		// Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
		if (word[i] == "a" ||
			word[i] == "e" ||
			word[i] == "i" ||
			word[i] == "o" ||
			word[i] == "u"
		) {
			continue;

		// Create an else statement that will add the letter to the second variable.
		} else {
			cons += word[i];				
		};
	};

	console.log(cons);

	


	